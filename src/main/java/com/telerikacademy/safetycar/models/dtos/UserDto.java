package com.telerikacademy.safetycar.models.dtos;

import com.sun.istack.NotNull;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull
    @Size(min = 2, max = 15, message = "First Name should be between 2 and 15 symbols!")
    private String firstName;
    @NotNull
    @Size(min = 2, max = 15, message = "Last Name should be between 2 and 15 symbols!")
    private String lastName;
    @NotNull
    @Size(min = 10, max = 15, message = "Phone should be between 10 and 15 symbols!")
    private String phone;
    @NotNull
    @Size(min = 5, max = 100, message = "Address should be between 5 and 100 symbols!")
    private String address;

    private String username;

    @Positive(message = "City id must be a positive number")
    private int cityId;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
