package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.models.enums.Status;

import java.util.Date;

public class SortPolicyDto {

    public int id;
    public String startDate;
    public Status status;
    public Date requestDate;

    public SortPolicyDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }
}
