package com.telerikacademy.safetycar.models.dtos;

import org.springframework.context.annotation.Scope;

import javax.validation.constraints.*;

@Scope("session")
public class SimulationFormDto {
   // private FormService formService;

    @Positive(message = "Model id must be a positive number")
    int modelId;

    @Min(value = 18, message = "Age should not be less than 18")
    @Max(value = 150, message = "Age should not be greater than 150")
    int driverAge;


    boolean accident;

    @NotEmpty
    String firstRegistrationDate;

    @PositiveOrZero(message = "Cubic capacity must be a positive number")
    int cubicCapacity;


    public SimulationFormDto() {
    }


    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isAccident() {
        return accident;
    }

    public void setAccident(boolean accident) {
        this.accident = accident;
    }

    public String getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(String firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }


}
