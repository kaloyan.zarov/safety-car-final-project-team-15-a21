package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.validation.PasswordMatches;
import com.telerikacademy.safetycar.validation.ValidEmail;


import javax.validation.constraints.*;

@PasswordMatches
public class UserRegistrationDto {

    @NotNull
    @ValidEmail(message = "Invalid email")
    private String username;


    @Size(min = 2, max = 20, message = "Password name should be between 10 & 14 symbols")
    private String password;

    @NotEmpty
    private String passwordConfirmation;


    @Size(min = 2, max = 20, message = "First name should be between 2 & 20 symbols")
    private String firstName;


    @Size(min = 2, max = 20, message = "Last name should be between 2 & 20 symbols")
    private String lastName;


    @Size(min = 2, max = 20, message = "Address name should be between 2 & 20 symbols")
    private String address;


    @Size(min = 10, max = 14, message = "Phone number should be between 10 & 14 symbols")
    private String phone;

    @Positive(message = "City id must be a positive number")
    @NotNull
    private int cityId;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
