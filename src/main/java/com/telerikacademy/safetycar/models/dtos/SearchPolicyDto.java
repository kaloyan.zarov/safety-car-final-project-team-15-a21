package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.models.enums.Status;

public class SearchPolicyDto {

    String fromDate;
    String toDate;
    Status status;
    String username;


    public SearchPolicyDto() {
    }


    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
