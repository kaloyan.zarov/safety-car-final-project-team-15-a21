package com.telerikacademy.safetycar.models.dtos;

import com.telerikacademy.safetycar.models.enums.Status;
import org.springframework.context.annotation.Scope;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Scope("session")
public class PolicyDto {


    private int userId;

    //@NotEmpty
    private String startDate;

    private int formId;

    //@NotEmpty
    private Status status;

    // @NotEmpty
    private MultipartFile picture;

    // @NotEmpty
    private LocalDateTime requestDate;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MultipartFile getPicture() {
        return picture;
    }

    public void setPicture(MultipartFile picture) {
        this.picture = picture;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }
}
