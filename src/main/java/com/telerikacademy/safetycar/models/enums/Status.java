package com.telerikacademy.safetycar.models.enums;

public enum Status {
    PENDING, APPROVED, CANCELED, REJECTED;
}
