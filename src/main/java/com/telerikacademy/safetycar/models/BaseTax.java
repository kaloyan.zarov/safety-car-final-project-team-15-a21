package com.telerikacademy.safetycar.models;

import javax.persistence.*;

@Entity
@Table(name = "base_tax_calculation")
public class BaseTax {


    public BaseTax() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int BaseId;

    @Column(name = "cc_min")
    int ccMin;

    @Column(name = "cc_max")
    int ccMax;

    @Column(name = "car_age_min")
    int carAgeMin;

    @Column(name = "car_age_max")
    int carAgeMax;

    @Column(name = "base_amount")
    double baseAmount;

    public int getBaseId() {
        return BaseId;
    }

    public void setBaseId(int baseId) {
        BaseId = baseId;
    }

    public int getCcMin() {
        return ccMin;
    }

    public void setCcMin(int ccMin) {
        this.ccMin = ccMin;
    }

    public int getCcMax() {
        return ccMax;
    }

    public void setCcMax(int ccMax) {
        this.ccMax = ccMax;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public void setCarAgeMin(int carAgeMin) {
        this.carAgeMin = carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public void setCarAgeMax(int carAgeMax) {
        this.carAgeMax = carAgeMax;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }
}
