package com.telerikacademy.safetycar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.safetycar.models.enums.Status;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "policies")
public class Policy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_id")
    private int id;

    @Column(name = "picture")
    private byte[] picture;

    @Column(name = "start_date")
    private String startDate;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetails owner;

    @OneToOne
    @JoinColumn(name = "form_id")
    private SimulationForm simulationForm;

    @JsonIgnore
    @Column
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "status")
    private Status status;

    @Column(name = "request_date")
    private LocalDateTime requestDate;

    public Policy() {
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public UserDetails getOwner() {
        return owner;
    }

    public void setOwner(UserDetails owner) {
        this.owner = owner;
    }

    public SimulationForm getSimulationForm() {
        return simulationForm;
    }

    public void setSimulationForm(SimulationForm simulationForm) {
        this.simulationForm = simulationForm;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
