package com.telerikacademy.safetycar.models.common;

public class Constants {

    //FormService
    public static final double ACCIDENT_COEFFICIENT = 1.2;
    public static final double DRIVER_AGE_COEFFICIENT = 1.05;
    public static final int DEFAULT_ACCIDENT_COEFFICIENT = 1;
    public static final int DEFAULT_DRIVER_AGE_COEFFICIENT = 1;
}
