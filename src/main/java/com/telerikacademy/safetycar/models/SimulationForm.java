package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "simulation_form")
public class SimulationForm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "form_id")
    private int formId;

    @Column(name = "first_reg_date")
    private LocalDate firstRegistrationDate;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "accidents_in_previous_year")
    private boolean accidents;

    @Column(name = "total_price")
    private double totalPrice;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id")
    private CarModel model;

    public SimulationForm() {
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isAccidents() {
        return accidents;
    }

    public void setAccidents(boolean accidents) {
        this.accidents = accidents;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(LocalDate firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimulationForm that = (SimulationForm) o;
        return formId == that.formId &&
                cubicCapacity == that.cubicCapacity &&
                driverAge == that.driverAge &&
                accidents == that.accidents &&
                Double.compare(that.totalPrice, totalPrice) == 0 &&
                Objects.equals(firstRegistrationDate, that.firstRegistrationDate) &&
                Objects.equals(model, that.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(formId, firstRegistrationDate, cubicCapacity, driverAge, accidents, totalPrice, model);
    }
}