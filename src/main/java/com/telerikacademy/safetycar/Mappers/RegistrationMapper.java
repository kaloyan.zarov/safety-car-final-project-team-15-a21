package com.telerikacademy.safetycar.Mappers;

import com.telerikacademy.safetycar.models.dtos.UserRegistrationDto;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.services.contracts.CitiesService;

public class RegistrationMapper {


    public static UserDetails fromDtoToUserDetails(UserRegistrationDto dto, CitiesService citiesService) {


        UserDetails userDetails = new UserDetails();
        userDetails.setUsername(dto.getUsername());
        userDetails.setFirstName(dto.getFirstName());
        userDetails.setLastName(dto.getLastName());
        userDetails.setAddress(dto.getAddress());
        userDetails.setPhone(dto.getPhone());
        userDetails.setCity(citiesService.getCityById(dto.getCityId()));

        return userDetails;
    }
}
