package com.telerikacademy.safetycar.Mappers;

import com.telerikacademy.safetycar.models.dtos.UserDto;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.services.contracts.CitiesService;

public class UserMapper {
    public static UserDetails fromDtoToUserDetails(UserDto dto, CitiesService citiesService, UserDetails userDetails) {


        userDetails.setFirstName(dto.getFirstName());
        userDetails.setLastName(dto.getLastName());
        userDetails.setAddress(dto.getAddress());
        userDetails.setPhone(dto.getPhone());
        userDetails.setCity(citiesService.getCityById(dto.getCityId()));

        return userDetails;
    }
}
