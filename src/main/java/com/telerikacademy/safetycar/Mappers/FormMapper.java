package com.telerikacademy.safetycar.Mappers;

import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;

import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.services.contracts.CarModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;

public class FormMapper {

    public static SimulationForm toSimulationForm(SimulationFormDto simulationFormDto, CarModelService carModelService) throws ParseException {
        SimulationForm simulationForm = new SimulationForm();
        simulationForm.setFirstRegistrationDate(new SimpleDateFormat("yyyy-MM-dd").parse(simulationFormDto.getFirstRegistrationDate()).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate());
        simulationForm.setDriverAge(simulationFormDto.getDriverAge());
        simulationForm.setAccidents(simulationFormDto.isAccident());
        simulationForm.setCubicCapacity(simulationFormDto.getCubicCapacity());
        simulationForm.setModel(carModelService.getById(simulationFormDto.getModelId()));
        return simulationForm;
    }
}
