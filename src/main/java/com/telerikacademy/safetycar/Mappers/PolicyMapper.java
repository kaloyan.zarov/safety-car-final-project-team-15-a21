package com.telerikacademy.safetycar.Mappers;

import com.telerikacademy.safetycar.models.dtos.PolicyDto;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.models.enums.Status;
import com.telerikacademy.safetycar.services.contracts.FormService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.time.LocalDateTime;

public class PolicyMapper {

    public static Policy fromDtoToPolicy(PolicyDto dto, UserService userService, FormService formService) throws IOException {
        UserDetails userDetails = new UserDetails();
        userDetails = userService.getById(dto.getUserId());

        SimulationForm form = new SimulationForm();
        form = formService.getById(dto.getFormId());
        MultipartFile picture = dto.getPicture();
        Policy policy = new Policy();
        policy.setStartDate(dto.getStartDate());
        policy.setStatus(Status.PENDING);
        policy.setRequestDate(LocalDateTime.now());
        policy.setPicture(picture.getBytes());
        policy.setOwner(userDetails);
        policy.setSimulationForm(form);

        return policy;
    }

    public static Policy fromDtoToRestPolicy(PolicyDto dto, UserService userService, FormService formService) throws IOException {
        UserDetails userDetails = new UserDetails();
        userDetails = userService.getById(dto.getUserId());

        SimulationForm form = new SimulationForm();
        form = formService.getById(dto.getFormId());
        MultipartFile picture = dto.getPicture();
        Policy policy = new Policy();
        policy.setStartDate(dto.getStartDate());
        policy.setStatus(Status.PENDING);
        policy.setRequestDate(LocalDateTime.now());
//        policy.setPicture(picture.getBytes());
        policy.setOwner(userDetails);
        policy.setSimulationForm(form);

        return policy;
    }
}
