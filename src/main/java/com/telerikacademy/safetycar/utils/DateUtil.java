package com.telerikacademy.safetycar.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {

    private DateUtil() {
    }

    public static int parseAge(String firstRegistration) throws ParseException {
        LocalDate currentDate = getCurrentDate();
        LocalDate firstRegistrationDate = getDateOfFirstRegistration(firstRegistration);
        Period period = Period.between(currentDate, firstRegistrationDate);
        return Math.abs(period.getYears());
    }

    private static LocalDate getDateOfFirstRegistration(String firstRegistrationDate) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(firstRegistrationDate).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private static LocalDate getCurrentDate() {
        return new Date().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
