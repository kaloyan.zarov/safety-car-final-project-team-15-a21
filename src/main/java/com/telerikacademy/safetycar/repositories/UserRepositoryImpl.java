package com.telerikacademy.safetycar.repositories;


import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(UserDetails.class, id);
        }
    }

    @Override
    public List<Policy> getUserPolicies(UserDetails userdetails){

        try(Session session=sessionFactory.openSession()){
            Query<Policy> query=session.createQuery("from Policy where owner.id= :user_id order by requestDate asc", Policy.class);
            query.setParameter("user_id", userdetails.getId());
            return query.list();
        }
    }

    public void createUserDetails(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userDetails);
        }
    }

    @Override
    public UserDetails getDetailsByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where username like :username", UserDetails.class);
            query.setParameter("username", username);
            List<UserDetails> userDetails = query.list();
            if (userDetails.isEmpty()) {
                throw new EntityNotFoundException(String.format("User with username %s not found.", username));
            }
            return userDetails.get(0);
        }
    }

    @Override

    public void update(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }

    }

    @Override //TODO entity not found validation
    public void softDelete(UserDetails userDetails) {
        User user = getUserByUsername(userDetails.getUsername());
        user.setEnabled(false);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {

            String username= user.getUsername();
            session.save(user);
            UserDetails userDetails = new UserDetails();
            userDetails.setUsername(user.getUsername());
            createUserDetails(userDetails);
            Query<UserDetails> query = session.createQuery("select u from UserDetails u where u.username = :username", UserDetails.class);
            query.setParameter("username", username);


        }
    }

    @Override
    public void saveUser(User user){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("User with username %s not found!", username));
            }
            return users.get(0);
        }
    }

    @Override
    public List<User> filterByUsername(String username) {
        if (username.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username like concat('%',:username,'%') order by username desc", User.class);
            query.setParameter("username", username);
            return query.list();
        }
    }
}


