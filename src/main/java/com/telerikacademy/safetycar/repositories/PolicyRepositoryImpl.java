package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public class PolicyRepositoryImpl implements PolicyRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Policy> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy ", Policy.class);
            return query.list();
        }
    }

    @Override
    public Policy getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Policy.class, id);
        }
    }

    @Override
    public void savePolicy(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policy);
        }
    }

    @Override
    public void updatePolicy(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policy);
            session.getTransaction().commit();
        }
    }
}





