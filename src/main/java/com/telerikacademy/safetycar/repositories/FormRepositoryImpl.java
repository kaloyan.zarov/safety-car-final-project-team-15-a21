package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.BaseTax;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.repositories.contracts.FormRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FormRepositoryImpl implements FormRepository {
    private final SessionFactory sessionFactory;


    @Autowired
    public FormRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    @Override
    public List<SimulationForm> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<SimulationForm> query = session.createQuery("from SimulationForm ", SimulationForm.class);
            return query.list();
        }
    }

    @Override
    public SimulationForm getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(SimulationForm.class, id);
            }
        }

    @Override
    public void saveForm(SimulationForm form) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(form);
            session.getTransaction().commit();
        }

    }

    @Override
    public double calculateBaseAmount(int cubicCapacity, int age) {
        try (Session session = sessionFactory.openSession()) {
            Query<BaseTax> query = session.createQuery("From BaseTax where " +
                            "(:cubicCapacity between ccMin and ccMax) and (:age between carAgeMin and carAgeMax)"
                    , BaseTax.class);
            query.setParameter("age", age);
            query.setParameter("cubicCapacity", cubicCapacity);

            return query.list().get(0).getBaseAmount();
        }
    }
}
