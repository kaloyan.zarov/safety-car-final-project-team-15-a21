package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.contracts.CarModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarModelRepositoryImpl implements CarModelRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CarModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarModel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel ", CarModel.class);
            return query.list();
        }
    }

    @Override
    public List<Make> getAllMakes() {
        try (Session session = sessionFactory.openSession()) {
            Query<Make> query = session.createQuery("from Make", Make.class);
            return query.list();
        }
    }

    @Override
    public CarModel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(CarModel.class, id);
        }
    }

    @Override
    public List<CarModel> getModelByMakeId(int makeId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where make.id = :make_id", CarModel.class);
            query.setParameter("make_id", makeId);
            return query.list();
        }
    }
}
