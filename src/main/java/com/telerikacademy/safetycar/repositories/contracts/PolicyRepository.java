package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Policy;

import java.util.List;

public interface PolicyRepository {
    List<Policy> getAll();

    Policy getById(int id);

    void savePolicy(Policy policy);

    void updatePolicy(Policy policy);

}

