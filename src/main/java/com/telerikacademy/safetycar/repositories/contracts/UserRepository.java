package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    UserDetails getById(int id);

    List<Policy> getUserPolicies(UserDetails userdetails);

    void createUser(User user);

    void saveUser(User user);

    void createUserDetails(UserDetails userDetails);

    UserDetails getDetailsByUsername(String username);

    List<User> filterByUsername(String username);

    void update(UserDetails userDetails);

    void softDelete(UserDetails userDetails);

    User getUserByUsername(String username);

}
