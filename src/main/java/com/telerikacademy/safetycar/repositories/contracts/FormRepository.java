package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.SimulationForm;

import java.util.List;

public interface FormRepository {
    List<SimulationForm> getAll();

    SimulationForm getById(int id);

    void saveForm(SimulationForm form);

    double calculateBaseAmount(int cubicCapacity, int age);

}
