package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.Make;

import java.util.List;

public interface CarModelRepository {
    List<CarModel> getAll();

    List<Make> getAllMakes();

    CarModel getById(int id);

    List<CarModel> getModelByMakeId(int makeId);


}
