package com.telerikacademy.safetycar.repositories.contracts;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CitiesRepository {
    List<City> getAll();

    City getCityById(int id);
}
