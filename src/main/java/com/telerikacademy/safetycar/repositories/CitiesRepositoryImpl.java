package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.repositories.contracts.CitiesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CitiesRepositoryImpl implements CitiesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CitiesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City ", City.class);
            return query.list();
        }
    }

    @Override
    public City getCityById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(City.class, id);
        }
    }
}
