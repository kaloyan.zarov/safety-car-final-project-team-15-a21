package com.telerikacademy.safetycar.exceptions;

public class UnautorizedOperationException extends RuntimeException {

    public UnautorizedOperationException(String message) {
        super(message);
    }
}
