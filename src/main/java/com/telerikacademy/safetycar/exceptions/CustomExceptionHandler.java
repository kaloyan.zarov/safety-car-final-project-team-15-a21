package com.telerikacademy.safetycar.exceptions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(basePackages = "com.telerikacademy.safetycar.controllers")
public class CustomExceptionHandler {
    private final Log LOGGER = LogFactory.getLog(CustomExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ModelAndView handleEntityNotFound(EntityNotFoundException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/errors/404");
        mav.addObject("message", e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ExceptionHandler(MultipartException.class)
    public String handleError1(MultipartException e, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:/";

    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:/";

    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {UnautorizedOperationException.class})
    public ModelAndView handleEntityNotFound(UnautorizedOperationException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/errors/access-denied");
        mav.addObject("message", e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

}
