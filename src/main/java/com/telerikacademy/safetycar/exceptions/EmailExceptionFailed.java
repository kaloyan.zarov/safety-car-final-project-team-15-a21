package com.telerikacademy.safetycar.exceptions;

public class EmailExceptionFailed extends RuntimeException{
    public EmailExceptionFailed(String message) {
        super(message);
    }
}
