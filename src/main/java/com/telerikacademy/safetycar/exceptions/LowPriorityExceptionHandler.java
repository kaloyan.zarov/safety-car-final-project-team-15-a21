package com.telerikacademy.safetycar.exceptions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Order
@ControllerAdvice(basePackages = "com.telerikacademy.safetycar.controllers")
public class LowPriorityExceptionHandler {
    private final Log LOGGER = LogFactory.getLog(Exception.class);

    public ModelAndView handleAllExceptions(HttpServletResponse response, HttpServletRequest request, Exception e) {
        ModelAndView mav = new ModelAndView();
        Integer statusCode = response.getStatus();
        mav.setViewName("error");
        mav.addObject("message", e.getMessage());
        mav.addObject("status", statusCode);
//        LOGGER.warn(e);
//        return mav;
        LOGGER.info(response.getStatus());
//    LOGGER.info(request);
        LOGGER.error("Request " + request.getRequestURL() + " Threw an exception", e);
        return mav;

    }
}
