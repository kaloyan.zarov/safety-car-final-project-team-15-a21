package com.telerikacademy.safetycar.controllers;


import com.telerikacademy.safetycar.exceptions.UserAlreadyExistException;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDto;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.registration.OnRegistrationCompleteEvent;
import com.telerikacademy.safetycar.models.User;

import com.telerikacademy.safetycar.services.contracts.EmailService;
import com.telerikacademy.safetycar.services.contracts.CitiesService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;


@Controller
public class RegistrationController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final UserService userService;
    private final CitiesService citiesService;
    private final ApplicationEventPublisher eventPublisher;
    private final MessageSource messages;
    public SimpleMailMessage template;
    public EmailService emailService;

    public RegistrationController(UserService userService, CitiesService citiesService,
                                  ApplicationEventPublisher eventPublisher, @Qualifier("messageSource") MessageSource messages,
                                  SimpleMailMessage template, EmailService emailService) {
        this.userService = userService;
        this.citiesService = citiesService;
        this.eventPublisher = eventPublisher;
        this.messages = messages;
        this.template = template;
        this.emailService = emailService;
    }


    @GetMapping("/register")
    public String showRegistrationForm(final HttpServletRequest request, final Model model) {
        LOGGER.debug("Rendering registration page.");
        final UserRegistrationDto accountDto = new UserRegistrationDto();
        model.addAttribute("user", accountDto);
        model.addAttribute("cities", citiesService.getAll());
        return "register";
    }


    @PostMapping("/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid UserRegistrationDto userRegistrationDto, BindingResult bindingResult,
            HttpServletRequest request, Model model) {
        ModelAndView mav;

        if (bindingResult.hasErrors()) {
            return new ModelAndView("register", bindingResult.getModel());
        }


        try {
            model.addAttribute("cities", citiesService.getAll());
            User registered = userService.registerNewUserAccount(userRegistrationDto);
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
        } catch (UserAlreadyExistException uaeEx) {
            mav = new ModelAndView("register", "user", userRegistrationDto);
            mav.addObject("message", "An account for that username/email already exists.");
            return mav;
        }

        return new ModelAndView("successRegister", "user", userRegistrationDto);
    }


    @GetMapping("/register-confirmation")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {

        Locale locale = request.getLocale();

        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/badUser?lang=" + locale.getLanguage();
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = messages.getMessage("auth.message.expired", null, locale);
            model.addAttribute("message", messageValue);
            return "redirect:/badUser?lang=" + locale.getLanguage();
        }

        user.setEnabled(true);
        userService.saveRegisteredUser(user);
        return "redirect:/";
    }
}
