package com.telerikacademy.safetycar.controllers;


import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;

import com.telerikacademy.safetycar.services.contracts.CarModelService;
import com.telerikacademy.safetycar.services.contracts.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.text.ParseException;


@Controller
@Scope("session")
public class FormController {
    private final FormService formService;
    private final CarModelService modelService;

    @Autowired
    public FormController(FormService formService, CarModelService modelService) {
        this.formService = formService;
        this.modelService = modelService;
    }

    @GetMapping("/forms")
    public String getFormPage(SimulationFormDto dto, Model model) {
        model.addAttribute("dto", dto);
        model.addAttribute("models", modelService.getAll());

        model.addAttribute("makes", modelService.getAllMakes());
        return "formPage";
    }

    @PostMapping("/forms/calculation")
    public String calculateTotalPrice(@Valid SimulationFormDto dto, BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("dto", dto);
            return "redirect:/forms";
        }
        model.addAttribute("dto", dto);

        try {
            formService.calculateTotalPrice(dto);

            return "redirect:/forms";
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, e.getMessage());
        }

    }

    @PostMapping("/forms/save")
    public String saveForm(@Valid SimulationFormDto dto, BindingResult bindingResult, ModelMap model, HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("dto", dto);
            return "redirect:/forms";
        }

        try {
            request.getSession().setAttribute("simulationFormSession", dto);
            request.getSession().setAttribute("totalPrice", formService.calculateTotalPrice(dto));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, e.getMessage());
        }
        return "redirect:/policy";
    }
}
