package com.telerikacademy.safetycar.controllers.restControllers;

import com.telerikacademy.safetycar.Mappers.PolicyMapper;
import com.telerikacademy.safetycar.models.dtos.PolicyDto;
import com.telerikacademy.safetycar.models.dtos.SearchPolicyDto;
import com.telerikacademy.safetycar.models.Policy;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.services.contracts.FormService;
import com.telerikacademy.safetycar.services.contracts.PolicyService;

import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/policies")
public class PolicyRestController {

    private final PolicyService policyService;
    private final UserService userService;
    private final FormService formService;

    @Autowired
    public PolicyRestController(PolicyService policyService, UserService userService, FormService formService) {
        this.policyService = policyService;
        this.userService = userService;
        this.formService = formService;
    }

    @GetMapping
    public List<Policy> getAll() {
        return policyService.getAll();
    }

    @GetMapping("/{id}")
    public Policy getById(@PathVariable int id) {
        try {
            return policyService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<Policy> search(
            @RequestBody SearchPolicyDto policyDto) {
        if (policyDto.getUsername() != null && policyDto.getFromDate() != null && policyDto.getToDate() != null &&
                policyDto.getStatus() != null) {
            return policyService.search(policyDto.getUsername(), policyDto.getFromDate(),
                    policyDto.getToDate(), policyDto.getStatus());
        } else
            return policyService.getAll();
    }

    @PostMapping
    public void savePolicy(@Valid @RequestBody PolicyDto policyDto) {
        try {

            Policy policy = PolicyMapper.fromDtoToRestPolicy(policyDto, userService, formService);
            policy.setSimulationForm(formService.getById(policyDto.getFormId()));
            policyService.savePolicyRest(policy);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PutMapping("/{id}")
    public void updatePolicy(@PathVariable int id, @Valid @RequestBody PolicyDto policyDto, Principal principal) {
        try {

            User user = userService.getUserByUsername(principal.getName());
            Policy policyToUpdate = policyService.getById(id);
            policyToUpdate = PolicyMapper.fromDtoToRestPolicy(policyDto, userService, formService);
            policyService.updatePolicy(policyToUpdate, user);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
