package com.telerikacademy.safetycar.controllers.restControllers;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.services.contracts.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarModelRestController {
    private final CarModelService carModelService;

    @Autowired
    public CarModelRestController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @GetMapping
    public List<CarModel> getAll() {
        return carModelService.getAll();
    }

    @GetMapping("/makes")
    public List<Make> getAllMakes() {
        return carModelService.getAllMakes();
    }

    @GetMapping("/{id}")
    public CarModel getById(@PathVariable int id) {
        try {
            return carModelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/make/{id}")
    public List<CarModel> getModelByMakeId(@PathVariable int id) {
        try {
            return carModelService.getModelByMakeId(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
