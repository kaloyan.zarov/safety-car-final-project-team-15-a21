package com.telerikacademy.safetycar.controllers.restControllers;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.services.contracts.CitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class CitiesRestController {
    private final CitiesService citiesService;

    @Autowired
    public CitiesRestController(CitiesService citiesService) {
        this.citiesService = citiesService;
    }

    @GetMapping
    public List<City> getAll() {
        return citiesService.getAll();
    }

    @GetMapping("/{id}")
    public City getCityById(@PathVariable int id) {
        try {
            return citiesService.getCityById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
