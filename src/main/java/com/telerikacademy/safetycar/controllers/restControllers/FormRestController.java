package com.telerikacademy.safetycar.controllers.restControllers;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.services.contracts.CarModelService;
import com.telerikacademy.safetycar.services.contracts.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.List;

import static com.telerikacademy.safetycar.utils.DateUtil.parseAge;

@RestController
@RequestMapping("/api/forms")
public class FormRestController {

    private final FormService formService;

    private final CarModelService carModelService;

    @Autowired
    public FormRestController(FormService formService, CarModelService carModelService) {
        this.formService = formService;
        this.carModelService = carModelService;
    }

    @GetMapping("/calculate")
    public double calculateTotalPrice(@RequestParam("cubicCapacity") int cubicCapacity, @RequestParam("firstRegistrationDate") String firstRegistrationDate, @RequestParam("driveAge") int driveAge, @RequestParam("accidents") boolean accidents) throws ParseException {
        double accidentCoefficient;
        double driverAgeCoefficient;
        int age = parseAge(firstRegistrationDate);


        if (accidents) {
            accidentCoefficient = 1.2;
        } else {
            accidentCoefficient = 1;
        }
        if (driveAge < 25) {
            driverAgeCoefficient = 1.05;
        } else {
            driverAgeCoefficient = 1;
        }
        double ba = (formService.calculateBaseAmount(cubicCapacity, age));
        BigDecimal bd = new BigDecimal(ba * accidentCoefficient * driverAgeCoefficient * 1.1).setScale(2, RoundingMode.HALF_UP);

        return bd.doubleValue();

    }

    @GetMapping
    public List<SimulationForm> getAll() {
        return formService.getAll();
    }


    @GetMapping("/{id}")
    public SimulationForm getById(@PathVariable int id) {
        try {
            return formService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/getModelByMake/{id}")
    public List<CarModel> getModelByMakeId(@PathVariable("id") int makeId) {
        return carModelService.getModelByMakeId(makeId);
    }

    @PostMapping
    public void saveSimulateForm(@Valid @RequestBody SimulationFormDto simulationFormDto) {
        try {
            formService.saveForm(simulationFormDto);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


}
