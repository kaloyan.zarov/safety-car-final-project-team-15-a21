package com.telerikacademy.safetycar.controllers.restControllers;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService userService;

    @Autowired
    public UserRestController(UserService service) {
        this.userService = service;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/policies/{username}")
    List<Policy> sortUserPolicies(@PathVariable String username) {
        try {
            return userService.getUserPolicies(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public UserDetails createUserDetails(@Valid @RequestBody UserDetails userDetails) {
        try {
            userService.createUserDetails(userDetails);
            return userDetails;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserDetails update(@PathVariable int id, Principal principal) {
        UserDetails userDetails = userService.getById(id);
        User user = userService.getUserByUsername(principal.getName());
        try {
            userService.update(userDetails, user);
            return userDetails;
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void softDelete(@PathVariable int id) {
        try {
            UserDetails user = userService.getById(id);
            userService.softDelete(user);
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
