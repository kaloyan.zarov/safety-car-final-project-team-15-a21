package com.telerikacademy.safetycar.controllers;


import com.telerikacademy.safetycar.Mappers.ImageUtil;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.dtos.SearchPolicyDto;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import com.telerikacademy.safetycar.services.contracts.PolicyService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/administrator")
public class AdminController {

    private final PolicyService policyService;
    private final UserService userService;
    private final EmailService emailService;

    @Autowired
    public AdminController(PolicyService policyService, UserService userService, EmailService emailService) {
        this.policyService = policyService;
        this.userService = userService;
        this.emailService = emailService;
    }

    @GetMapping
    public String showAdminPage(Model model) {
        model.addAttribute("allPolicies", policyService.getAll());
        model.addAttribute("imgUtil", new ImageUtil());
        model.addAttribute("searchPolicyDto", new SearchPolicyDto());
        return "admin-portal";
    }

    @PostMapping
    public String searchForPolicies(@ModelAttribute SearchPolicyDto policyDto, Model model) {
        List<Policy> policyList = policyService.search(policyDto.getUsername(), policyDto.getFromDate(),
                policyDto.getToDate(), policyDto.getStatus());
        getPolicyResults(model, policyList, policyDto);
        return "admin-portal";
    }

    @PostMapping("/approve/{id}")
    public String approvePolicy(@PathVariable int id, Principal principal) {


        try {
            policyService.approvePolicy(id, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        return "redirect:/administrator";
    }

    @PostMapping("/reject/{id}")
    public String rejectPolicy(@PathVariable int id, Principal principal) {


        try {
            policyService.rejectPolicy(id, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

        return "redirect:/administrator";
    }

    private void getPolicyResults(Model model, List<Policy> policies, SearchPolicyDto dto) {

        model.addAttribute("allPolicies", policies);
        model.addAttribute("imgUtil", new ImageUtil());
        model.addAttribute("searchPolicyDto", dto);
    }
}
