package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.Mappers.UserMapper;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.dtos.UserDto;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.services.contracts.CitiesService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {
    private final UserService userService;
    private final CitiesService citiesService;

    @Autowired
    public UserController(UserService userService, CitiesService citiesService) {
        this.userService = userService;
        this.citiesService = citiesService;
    }

    @GetMapping("/edit-profile")
    public String getEditProfile(Model model, UserDto dto, Principal principal) {

        model.addAttribute("userDetails", userService.getDetailsByUsername(principal.getName()));
        model.addAttribute("cities", citiesService.getAll());
        model.addAttribute("userDto", dto);

        return "editUser";
    }

    @PostMapping("/edit-profile")
    public String handleEditProfile(@Valid UserDto dto,
                                    BindingResult bindingResult,
                                    Model model, Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("userDto", dto);
            model.addAttribute("cities", citiesService.getAll());
            return "editUser";
        }
        User user = userService.getUserByUsername(principal.getName());
        UserDetails userToUpdate = userService.getDetailsByUsername(principal.getName());
        userService.update(UserMapper.fromDtoToUserDetails(dto, citiesService, userToUpdate), user);


        return "redirect:/";

    }


    @PostMapping("/delete/{id}")
    public String handleDeleteUser(@PathVariable int id) {
        try {
            UserDetails userDetails = userService.getById(id);
            userService.softDelete(userDetails);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

        return "redirect:/";
    }

    @GetMapping("/my-profile")
    public String getMyProfilePage(Principal principal, Model model) {

        String username = principal.getName();
        model.addAttribute("details", userService.getDetailsByUsername(username));
        model.addAttribute("policies", userService.getUserPolicies(username));

        return "my-profile";
    }

    @GetMapping("/admin-portal")
    public String getAdminPortalPage(Principal principal, Model model) {
        //TODO after policies
        return "admin-portal";
    }


}
