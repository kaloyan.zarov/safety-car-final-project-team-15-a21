package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.dtos.PolicyDto;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.services.contracts.FormService;
import com.telerikacademy.safetycar.services.contracts.PolicyService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;

@Controller
@Scope("session")
public class PolicyController {
    private final PolicyService policyService;
    private final UserService userService;
    private final FormService formService;

    @Autowired
    public PolicyController(PolicyService policyService, UserService userService, FormService formService) {
        this.policyService = policyService;
        this.userService = userService;
        this.formService = formService;
    }

    @GetMapping("/policy")
    public String getPolicyPage(PolicyDto policyDto, Principal principal, ModelMap model) {
        model.addAttribute("policyDto", policyDto);
        model.addAttribute("user", userService.getDetailsByUsername(principal.getName()));


        return "policy";
    }

    @PostMapping("/policy/cancel/{id}")
    public String cancelPolicy(@PathVariable int id, Principal principal) {

        policyService.cancelPolicy(id, principal);
        return "redirect:/my-profile";
    }

    @PostMapping("/policy/save")
    public String savePolicy(PolicyDto policyDto, SessionStatus sessionStatus, HttpSession session, Principal principal) throws ParseException, IOException {
        SimulationFormDto simForm = (SimulationFormDto) session.getAttribute("simulationFormSession");

        if (simForm == null) {
            return "redirect:/forms";
        }
        session.getAttribute("totalPrice");
        try {
            policyService.savePolicy(policyDto, principal, simForm);
            sessionStatus.setComplete();

        } catch (SizeLimitExceededException e) {
            String message = "File exceeds size limit.";
        }
        return "redirect:/";
    }


}

