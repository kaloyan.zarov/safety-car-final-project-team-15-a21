package com.telerikacademy.safetycar.controllers;

import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("simulationForm")
public class HomeController {

    @RequestMapping("/")
    public String showHomePage(Model model) {
        model.addAttribute("simulationForm", new SimulationFormDto());

        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPortal() {
        return "admin";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}
