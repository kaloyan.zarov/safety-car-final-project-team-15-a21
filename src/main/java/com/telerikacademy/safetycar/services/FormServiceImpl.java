package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.Mappers.FormMapper;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.repositories.contracts.FormRepository;
import com.telerikacademy.safetycar.services.contracts.CarModelService;
import com.telerikacademy.safetycar.services.contracts.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.List;

import static com.telerikacademy.safetycar.models.common.Constants.*;
import static com.telerikacademy.safetycar.utils.DateUtil.parseAge;

@Service
public class FormServiceImpl implements FormService {

    private final FormRepository repository;
    private final CarModelService modelService;
    private final FormRepository formRepository;

    @Autowired
    public FormServiceImpl(FormRepository repository, CarModelService modelService, FormRepository formRepository) {
        this.repository = repository;
        this.modelService = modelService;
        this.formRepository = formRepository;
    }

    @Override
    public List<SimulationForm> getAll() {
        return repository.getAll();
    }

    @Override
    public SimulationForm getById(int id) {
        SimulationForm form = formRepository.getById(id);
        if (form == null) {
            throw new EntityNotFoundException(
                    String.format("Form with id %d not found!", id));
        }
        return repository.getById(id);
    }

    @Override
    public SimulationForm saveForm(SimulationFormDto dto) throws ParseException {
        SimulationForm form = FormMapper.toSimulationForm(dto, modelService);
        form.setTotalPrice(calculateTotalPrice(dto));

        repository.saveForm(form);
        return form;
    }

    @Override
    public double calculateBaseAmount(int cubicCapacity, int age) {
        return repository.calculateBaseAmount(cubicCapacity, age);
    }

    @Override
    public double calculateTotalPrice(SimulationFormDto dto) throws ParseException {
        double accidentCoefficient = DEFAULT_ACCIDENT_COEFFICIENT;
        double driverAgeCoefficient = DEFAULT_DRIVER_AGE_COEFFICIENT;
        int cubicCapacity = dto.getCubicCapacity();
        int driverAge = dto.getDriverAge();
        boolean accident = dto.isAccident();
        int age = parseAge(dto.getFirstRegistrationDate());

        if (accident) {
            accidentCoefficient = ACCIDENT_COEFFICIENT;
        }
        if (driverAge < 25) {
            driverAgeCoefficient = DRIVER_AGE_COEFFICIENT;
        }
        double baseAmount = calculateBaseAmount(cubicCapacity, age);
        BigDecimal bigDecimal = new BigDecimal(baseAmount * accidentCoefficient * driverAgeCoefficient * 1.1).setScale(2, RoundingMode.HALF_UP);

        return bigDecimal.doubleValue();

    }
}
