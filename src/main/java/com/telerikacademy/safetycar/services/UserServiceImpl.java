package com.telerikacademy.safetycar.services;


import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.exceptions.UserAlreadyExistException;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDto;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.repositories.contracts.VerificationTokenRepository;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import com.telerikacademy.safetycar.services.contracts.CitiesService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.safetycar.Mappers.RegistrationMapper.fromDtoToUserDetails;

@Service
//@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final VerificationTokenRepository tokenRepository;
    private final UserDetailsManager userDetailsManager;
    private final CitiesService citiesService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, VerificationTokenRepository tokenRepository, UserDetailsManager userDetailsManager, CitiesService citiesService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = tokenRepository;
        this.userDetailsManager = userDetailsManager;
        this.citiesService = citiesService;
    }

    @Override
    public void saveRegisteredUser(final User user) {
        userRepository.saveUser(user);
    }


    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public UserDetails getById(int id) {
        UserDetails userDetails = userRepository.getById(id);
        if (userDetails == null) {
            throw new EntityNotFoundException(
                    String.format("User with id %d not found!", id));
        }
        return userRepository.getById(id);
    }

    @Override
    public void createUser(User user) {
        checkUsernameUnique(user);
        userRepository.createUser(user);
    }


    @Override
    public User getUser(String verificationToken) {
        User user = tokenRepository.findByToken(verificationToken).getUser();
        return user;
    }


    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);

        tokenRepository.save(myToken);
    }

    @Override
    public UserDetails getDetailsByUsername(String username) {
        return userRepository.getDetailsByUsername(username);
    }

    @Override
    public void update(UserDetails userDetails, User user) {
        checkIfAuthorized(user);
        userRepository.update(userDetails);

    }

    //@Transactional
    @Override
    public User registerNewUserAccount(UserRegistrationDto userDto)
            throws UserAlreadyExistException {

        if (emailExists(userDto.getUsername())) {
            throw new UserAlreadyExistException("There is an account with that email address: " + userDto.getUsername());
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getUsername(), passwordEncoder.encode(userDto.getPassword()),
                        false, true, true, true, authorities);
        userDetailsManager.createUser(newUser);
        User user = getUserByUsername(userDto.getUsername());
        UserDetails userDetails = fromDtoToUserDetails(userDto, citiesService);
        createUserDetails(userDetails);
        user.setUserDetails(userDetails);
        update(userDetails, user);

        return user;
    }

    @Override
    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }


    public boolean emailExists(final String username) {
        return userDetailsManager.userExists(username);
    }

    @Override
    public List<Policy> getUserPolicies(String username) {
        UserDetails userDetails = getDetailsByUsername(username);
        return userRepository.getUserPolicies(userDetails);
    }

    @Override
    public void createUserDetails(UserDetails userDetails) {
        userRepository.createUserDetails(userDetails);
    }

    @Override
    public void softDelete(UserDetails userDetails) {
        User user = getUserByUsername(userDetails.getUsername());
        checkIfAuthorized(user);
        userRepository.softDelete(userDetails);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }


    private void checkUsernameUnique(User user) {
        String username = user.getUsername();
        List<User> users = userRepository.filterByUsername(username);
        if (users.size() > 0) {
            throw new DuplicateEntityException(
                    String.format(
                            "User with username %s already exists!",
                            user.getUsername())
            );
        }
    }

    private void checkIfAuthorized(User user) {
        if (!user.isAdmin() &&
                !user.getUserDetails().getUsername().equals(user.getUsername())) {
            throw new InvalidOperationException("You are not allowed to do this operation");
        }
    }
}
