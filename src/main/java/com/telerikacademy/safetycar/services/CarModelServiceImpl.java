package com.telerikacademy.safetycar.services;


import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.contracts.CarModelRepository;
import com.telerikacademy.safetycar.services.contracts.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {
    private final CarModelRepository repository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<CarModel> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Make> getAllMakes() {
        return repository.getAllMakes();
    }

    @Override
    public CarModel getById(int id) {
        CarModel carModel = repository.getById(id);
        if (carModel == null) {
            throw new EntityNotFoundException(
                    String.format("Car model with id %d not found!", id));
        }
        return repository.getById(id);
    }

    @Override
    public List<CarModel> getModelByMakeId(int makeId) {
        return repository.getModelByMakeId(makeId);
    }
}
