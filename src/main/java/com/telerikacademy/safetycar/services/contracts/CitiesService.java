package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CitiesService {
    List<City> getAll();

    City getCityById(int id);
}
