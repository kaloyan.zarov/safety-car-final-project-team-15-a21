package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.exceptions.UserAlreadyExistException;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDto;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.models.VerificationToken;

import java.util.List;

public interface UserService {
    void createVerificationToken(User user, String token);

    User registerNewUserAccount(UserRegistrationDto accountDto) throws UserAlreadyExistException;

    VerificationToken getVerificationToken(String VerificationToken);

    void saveRegisteredUser(User user);

    User getUser(String verificationToken);

    List<User> getAll();

    UserDetails getById(int id);

    List<Policy> getUserPolicies(String username);

    void createUser(User user);

    void createUserDetails(UserDetails userDetails);

    UserDetails getDetailsByUsername(String username);

    void update(UserDetails userDetails,User user);

    void softDelete(UserDetails userDetails);

    User getUserByUsername(String username);

    boolean emailExists(final String username);

//    List<Policy> getUserPolicies(String username);
}
