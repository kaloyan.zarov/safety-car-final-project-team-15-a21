package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.dtos.PolicyDto;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.enums.Status;
import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;
import java.util.List;

public interface PolicyService {
    List<Policy> getAll();

    Policy getById(int id);

    void savePolicy(PolicyDto policyDto, Principal principal, SimulationFormDto simForm) throws ParseException, IOException;

    void savePolicyRest(Policy policy);

    void approvePolicy(int id, Principal principal);

    void rejectPolicy(int id, Principal principal);

    void cancelPolicy(int id, Principal principal);

    void updatePolicy(Policy policy, User user);
    List<Policy> search(String username, String fromDate, String toDate, Status status);

}