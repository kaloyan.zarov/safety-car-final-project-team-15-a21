package com.telerikacademy.safetycar.services.contracts;

import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.SimulationForm;

import java.text.ParseException;
import java.util.List;

public interface FormService {

    List<SimulationForm> getAll();

    SimulationForm getById(int id);

    SimulationForm saveForm(SimulationFormDto dto) throws ParseException;

    double calculateBaseAmount(int cubicCapacity, int age);

    double calculateTotalPrice(SimulationFormDto dto) throws ParseException;

}
