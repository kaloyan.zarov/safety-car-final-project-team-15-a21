package com.telerikacademy.safetycar.services.contracts;


import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.Make;

import java.util.List;

public interface CarModelService {
    List<CarModel> getAll();
    List<Make> getAllMakes();
    CarModel getById(int id);
    List<CarModel>getModelByMakeId(int makeId);

}
