package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.contracts.CitiesRepository;
import com.telerikacademy.safetycar.services.contracts.CitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CitiesServiceImpl implements CitiesService {
    private final CitiesRepository repository;

    @Autowired
    public CitiesServiceImpl(CitiesRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<City> getAll() {
        return repository.getAll();
    }

    @Override
    public City getCityById(int id) {
        City city = repository.getCityById(id);
        if (city == null) {
            throw new EntityNotFoundException(
                    String.format("City with id %d not found!", id));
        }
        return repository.getCityById(id);
    }
}
