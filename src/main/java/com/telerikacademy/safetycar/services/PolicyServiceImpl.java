package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.Mappers.PolicyMapper;
import com.telerikacademy.safetycar.models.dtos.PolicyDto;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.models.enums.Status;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRepository;
import com.telerikacademy.safetycar.services.contracts.FormService;
import com.telerikacademy.safetycar.services.contracts.EmailService;
import com.telerikacademy.safetycar.services.contracts.PolicyService;
import com.telerikacademy.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.security.Principal;
import java.io.IOException;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PolicyServiceImpl implements PolicyService {
    private final PolicyRepository policyRepository;
    private final EmailService emailService;
    private final UserService userService;
    private final FormService formService;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository, EmailService emailService, UserService userService, FormService formService) {
        this.policyRepository = policyRepository;
        this.emailService = emailService;
        this.userService = userService;
        this.formService = formService;

    }

    @Override
    public List<Policy> getAll() {
        return policyRepository.getAll();
    }

    @Override
    public Policy getById(int id) {
        Policy policy = policyRepository.getById(id);
        if (policy == null) {
            throw new EntityNotFoundException(
                    String.format("Policy with id %d not found!", id));
        }
        return policyRepository.getById(id);
    }

    @Override
    public void savePolicy(PolicyDto policyDto, Principal principal, SimulationFormDto simForm) throws ParseException, IOException {
        SimulationForm form = formService.saveForm(simForm);
        policyDto.setFormId(form.getFormId());
        policyDto.setUserId(userService.getDetailsByUsername(principal.getName()).getId());
        Policy policy = PolicyMapper.fromDtoToPolicy(policyDto, userService, formService);

        policyRepository.savePolicy(policy);
    }

    @Override
    public void savePolicyRest(Policy policy) {
        policyRepository.savePolicy(policy);
    }

    @Override
    public void approvePolicy(int id, Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        Policy policyToUpdate = getById(id);
        checkUserIsAdmin(user);
        policyToUpdate.setStatus(Status.APPROVED);
        policyRepository.updatePolicy(policyToUpdate);
        emailService.sendSimpleMessage(policyToUpdate.getOwner().getUsername(), "Safety car",
                "Your policy request has been approved");
    }

    @Override
    public void rejectPolicy(int id, Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        Policy policyToUpdate = getById(id);
        checkUserIsAdmin(user);
        policyToUpdate.setStatus(Status.REJECTED);
        policyRepository.updatePolicy(policyToUpdate);
        emailService.sendSimpleMessage(policyToUpdate.getOwner().getUsername(), "Safety car",
                "Your policy request has been rejected");
    }

    @Override
    public void cancelPolicy(int id, Principal principal) {

        User user = userService.getUserByUsername(principal.getName());
        Policy policyToUpdate = getById(id);
        checkUserCanEditPolicy(policyToUpdate, user);
        policyToUpdate.setStatus(Status.CANCELED);
        policyRepository.updatePolicy(policyToUpdate);
    }



    @Override
    public void updatePolicy(Policy policy, User user) {

        checkUserCanEditPolicy(policy, user);
        policyRepository.updatePolicy(policy);
    }


    @Override
    public List<Policy> search(String username, String fromDate, String toDate, Status status) {


        List<Policy> result = getAll();


        if (!username.isEmpty()) {
            result = result.stream()
                    .filter(policy -> policy.getOwner().getUsername().toLowerCase().contains(username.toLowerCase()))
                    .collect(Collectors.toList());
        }

        if (!fromDate.isEmpty()) {
            LocalDateTime dateTimeOne = LocalDateTime.parse(fromDate);
            result = result.stream().filter(policy -> policy.getRequestDate().isAfter(dateTimeOne)).collect(Collectors.toList());
        }

        if (!toDate.isEmpty()) {
            LocalDateTime dateTimeTwo = LocalDateTime.parse(toDate);
            result = result.stream().filter(policy -> policy.getRequestDate().isBefore(dateTimeTwo)).collect(Collectors.toList());
        }


        if (status != null) {
            switch (status) {
                case PENDING:
                    result = result.stream().filter(policy -> policy.getStatus() == Status.PENDING).collect(Collectors.toList());
                    break;
                case APPROVED:
                    result = result.stream().filter(policy -> policy.getStatus() == Status.APPROVED).collect(Collectors.toList());
                    break;
                case CANCELED:
                    result = result.stream().filter(policy -> policy.getStatus() == Status.CANCELED).collect(Collectors.toList());
                    break;
                case REJECTED:
                    result = result.stream().filter(policy -> policy.getStatus() == Status.REJECTED).collect(Collectors.toList());
                    break;

            }
        }


        return result;
    }

    private void checkUserIsAdmin(User user) {
        if (!user.isAdmin()) {
            throw new InvalidOperationException(
                    String.format(
                            "User %s can't edit policy",
                            user.getUsername()));
        }
    }

    private void checkUserCanEditPolicy(Policy policy,User user) {
        if (!user.isAdmin() &&
                policy.getOwner().getId() != user.getUserDetails().getId()){
            throw new InvalidOperationException(
                    String.format(
                            "User %s can't edit policy",
                            user.getUsername()));
        }
    }
}