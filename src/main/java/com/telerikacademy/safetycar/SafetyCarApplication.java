package com.telerikacademy.safetycar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SafetyCarApplication {

    public static void main(String[] args) {

        SpringApplication.run(SafetyCarApplication.class, args);

    }

}
