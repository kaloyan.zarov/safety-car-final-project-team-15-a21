package com.telerikacademy.safetycar.validation;

import com.telerikacademy.safetycar.models.dtos.UserRegistrationDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

        @Override
        public void initialize(PasswordMatches constraintAnnotation) {
        }
        @Override
        public boolean isValid(Object obj, ConstraintValidatorContext context){
            UserRegistrationDto userRegistrationDto = (UserRegistrationDto) obj;
            return userRegistrationDto.getPassword().equals(userRegistrationDto.getPasswordConfirmation());
        }
    }
