DROP DATABASE IF EXISTS `safety_car`;
CREATE DATABASE  IF NOT EXISTS `safety_car`;
USE `safety_car`;

create or replace table base_tax_calculation
(
    id int auto_increment
        primary key,
    cc_max int not null,
    car_age_min int not null,
    car_age_max int not null,
    base_amount double not null,
    cc_min int not null
);

create or replace table cities
(
    city_id int auto_increment
        primary key,
    name varchar(20) not null
);

create or replace table make
(
    make_id int(10) auto_increment
        primary key,
    title varchar(55) default '' not null
)
    charset=utf8;

create or replace table model
(
    model_id int(10) auto_increment
        primary key,
    make_id int(10) default 0 not null,
    title varchar(125) default '' not null,
    constraint model_make_make_id_fk
        foreign key (make_id) references make (make_id)
)
    charset=utf8;

create or replace table simulation_form
(
    driver_age int not null,
    accidents_in_previous_year tinyint not null,
    form_id int auto_increment
        primary key,
    total_price double not null,
    first_reg_date date null,
    cubic_capacity int null,
    model_id int null,
    constraint simulation_form_model_model_id_fk
        foreign key (model_id) references model (model_id)
);

create or replace table users
(
    username varchar(50) not null
        primary key,
    password varchar(20) not null,
    enabled tinyint not null
);

create or replace table authorities
(
    username varchar(50) not null,
    authority varchar(20) not null,
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

create or replace table users_details
(
    user_id int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    username varchar(50) not null,
    address varchar(50) null,
    phone varchar(20) not null,
    city_id int not null,
    constraint users_details_cities_city_id_fk
        foreign key (city_id) references cities (city_id),
    constraint users_details_users_username_fk
        foreign key (username) references users (username)
);

create or replace table policies
(
    policy_id int auto_increment
        primary key,
    user_id int not null,
    form_id int not null,
    picture longblob null,
    start_date date not null,
    status varchar(10) not null,
    request_date datetime null,
    constraint policies_simulation_form_form_id_fk
        foreign key (form_id) references simulation_form (form_id),
    constraint policies_users_details_user_id_fk
        foreign key (user_id) references users_details (user_id)
);

create or replace index users_details_addresses_address_id_fk
    on users_details (address);

create or replace table verification_token
(
    id int auto_increment
        primary key,
    username varchar(50) not null,
    token varchar(255) not null,
    expiry_date timestamp default current_timestamp() not null on update current_timestamp(),
    constraint verification_token_users_username_fk_2
        foreign key (username) references users (username)
);