package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidOperationException;
import com.telerikacademy.safetycar.models.Authority;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository mockUserRepository;

    @Test
    public void getAll_ShouldReturn_WhenAllUsersExist() {

        // Arrange
        List<User> list = new ArrayList<>();
        User user = new User();
        list.add(user);
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(list);

        //Act
        List<User> users = userService.getAll();

        //Assert
        Assertions.assertEquals(1, users.size()); // proverqvame dali razmera na users lista e = 1, za6toto v arrange ima samo 1 user
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllUsersDoNotExist() {

        // Arrange
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(Collections.emptyList());

        //Act
        List<User> list = userService.getAll();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_ShouldReturn_WhenUserIdExists() {

        //tuk testvame userDetails

        // Arrange
        UserDetails user = new UserDetails();
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        // Act
        UserDetails returnedUser = userService.getById(1); // davame su6toto id kato gore - 1

        //Assert
        Assertions.assertEquals(user, returnedUser); // sravnqvame suzdadeniq user s tozi koito trqbva da se vurne

    }

    @Test
    public void getById_ShouldThrow_WhenUserIdNotFound() {

        // Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())) // vzima koito i da e user
                .thenReturn(null);                                     // i mu kazva6 vurni null

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getById(1));                 //ako nqma id 1 da hvurli EntityNotFoundException
    }

    @Test
    public void createUserDetails_ShouldNotReturn_WhenUserExists() {

        // Arrange
        UserDetails user = new UserDetails();

        //Act
        userService.createUserDetails(user);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .createUserDetails(user);
    }

    @Test
    public void updateUserDetails_ShouldNotReturn_WhenIsAdmin() {

        // Arrange
        User user = new User();
        Authority authority = new Authority();
        Set<Authority> set = new HashSet<>();
        set.add(authority);
        authority.setAuthority("ROLE_ADMIN");
        UserDetails userDetails = new UserDetails();
        user.setAuthorities(set);

        //Act
        userService.update(userDetails, user);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(userDetails);
    }
}

