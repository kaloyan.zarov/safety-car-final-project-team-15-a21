package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.contracts.CitiesRepository;
import com.telerikacademy.safetycar.repositories.contracts.FormRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CitiesServiceTests {

    @InjectMocks
    CitiesServiceImpl citiesService;

    @Mock
    CitiesRepository mockCitiesRepository;

    @Test
    public void getAll_ShouldReturn_WhenAllCitiesExist() {

        // Arrange
        List<City> list = new ArrayList<>();
        City city = new City();
        list.add(city);
        Mockito.when(mockCitiesRepository.getAll())
                .thenReturn(list);

        //Act
        List<City> cities = citiesService.getAll();

        //Assert
        Assertions.assertEquals(1, cities.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllCitiesDoNotExist() {

        // Arrange
        Mockito.when(mockCitiesRepository.getAll())
                .thenReturn(Collections.emptyList());

        //Act
        List<City> list = citiesService.getAll();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_ShouldReturn_WhenCityIdExists() {

        // Arrange
        City city = new City();
        Mockito.when(mockCitiesRepository.getCityById(1))
                .thenReturn(city);

        // Act
        City returnedCity = citiesService.getCityById(1);

        //Assert
        Assertions.assertEquals(city, returnedCity);

    }

    @Test
    public void getById_ShouldThrow_WhenCityIdNotFound() {

        // Arrange
        Mockito.when(mockCitiesRepository.getCityById(Mockito.anyInt()))
                .thenReturn(null);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> citiesService.getCityById(1));
    }
}
