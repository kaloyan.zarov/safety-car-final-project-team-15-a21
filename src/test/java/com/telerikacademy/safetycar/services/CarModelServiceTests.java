package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.contracts.CarModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarModelServiceTests {

    @InjectMocks
    CarModelServiceImpl carModelService;

    @Mock
    CarModelRepository mockCarModelRepository;

    @Test
    public void getAll_ShouldReturn_WhenAllCarModelsExist() {

        // Arrange
        List<CarModel> list = new ArrayList<>();
        CarModel carModel = new CarModel();
        list.add(carModel);
        Mockito.when(mockCarModelRepository.getAll())
                .thenReturn(list);

        //Act
        List<CarModel> carModels = carModelService.getAll();

        //Assert
        Assertions.assertEquals(1, carModels.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllCarModelsDoNotExist() {

        // Arrange
        Mockito.when(mockCarModelRepository.getAll())
                .thenReturn(Collections.emptyList());

        //Act
        List<CarModel> list = carModelService.getAll();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getAll_ShouldReturn_WhenAllMakesExist() {

        // Arrange
        List<Make> list = new ArrayList<>();
        Make make = new Make();
        list.add(make);
        Mockito.when(mockCarModelRepository.getAllMakes())
                .thenReturn(list);

        //Act
        List<Make> carMakes = carModelService.getAllMakes();

        //Assert
        Assertions.assertEquals(1, carMakes.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllMakesDoNotExist() {

        // Arrange
        Mockito.when(mockCarModelRepository.getAllMakes())
                .thenReturn(Collections.emptyList());

        //Act
        List<Make> list = carModelService.getAllMakes();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_ShouldReturn_WhenCarModelIdExists() {

        // Arrange
        CarModel carModel = new CarModel();
        Mockito.when(mockCarModelRepository.getById(1))
                .thenReturn(carModel);

        // Act
        CarModel returnedCarModel = carModelService.getById(1);

        //Assert
        Assertions.assertEquals(carModel, returnedCarModel);

    }

    @Test
    public void getById_ShouldThrow_WhenCarModelIdNotFound() {

        // Arrange
        Mockito.when(mockCarModelRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> carModelService.getById(1));
    }

    @Test
    public void getByMakeId_ShouldReturn_ListOfMakes() {
        // Arrange
        List<CarModel> modelsList = new ArrayList<>();
        Make make = new Make();
        make.setId(1);
        CarModel carModel = new CarModel();
        carModel.setMake(make);
        modelsList.add(carModel);
        Mockito.when(mockCarModelRepository.getModelByMakeId(Mockito.anyInt()))
                .thenReturn(modelsList);
        // Act
        List<CarModel> models = carModelService.getModelByMakeId(1);
        // Assert
        Assertions.assertEquals(1, models.size());
    }
}
