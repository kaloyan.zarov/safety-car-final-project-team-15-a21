package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Policy;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserDetails;
import com.telerikacademy.safetycar.repositories.contracts.PolicyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PolicyServiceTests {

    @InjectMocks
    PolicyServiceImpl policyService;

    @Mock
    PolicyRepository mockPolicyRepository;

    @Test
    public void getAll_ShouldReturn_WhenAllPoliciesExist() {

        // Arrange
        List<Policy> list = new ArrayList<>();
        Policy policy = new Policy();
        list.add(policy);
        Mockito.when(mockPolicyRepository.getAll())
                .thenReturn(list);

        //Act
        List<Policy> policies = policyService.getAll();

        //Assert
        Assertions.assertEquals(1, policies.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllPoliciesDoNotExist() {

        // Arrange
        Mockito.when(mockPolicyRepository.getAll())
                .thenReturn(Collections.emptyList());

        //Act
        List<Policy> list = policyService.getAll();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_ShouldReturn_WhenPolicyIdExists() {

        // Arrange
        Policy policy = new Policy();
        Mockito.when(mockPolicyRepository.getById(1))
                .thenReturn(policy);

        // Act
        Policy returnedPolicy = policyService.getById(1);

        //Assert
        Assertions.assertEquals(policy, returnedPolicy);

    }

    @Test
    public void getById_ShouldThrow_WhenUserIdNotFound() {

        // Arrange
        Mockito.when(mockPolicyRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> policyService.getById(1));
    }
}
