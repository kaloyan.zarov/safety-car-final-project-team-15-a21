package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.models.SimulationForm;
import com.telerikacademy.safetycar.repositories.contracts.FormRepository;
import com.telerikacademy.safetycar.services.contracts.CarModelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.telerikacademy.safetycar.helpers.ObjectsFactory.getSimulationFormDto;
import static com.telerikacademy.safetycar.helpers.ObjectsFactory.toSimulationForm;
import static com.telerikacademy.safetycar.utils.DateUtil.parseAge;

@ExtendWith(MockitoExtension.class)
public class FormServiceTests {

    @InjectMocks
    FormServiceImpl formService;

    @Mock
    FormRepository mockFormRepository;

    @Mock
    CarModelService modelService;

    @Test
    public void getAll_ShouldReturn_WhenAllFormsExist() {

        // Arrange
        List<SimulationForm> list = new ArrayList<>();
        SimulationForm simulationForm = new SimulationForm();
        list.add(simulationForm);
        Mockito.when(mockFormRepository.getAll())
                .thenReturn(list);

        //Act
        List<SimulationForm> forms = formService.getAll();

        //Assert
        Assertions.assertEquals(1, forms.size());
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenAllFormsDoNotExist() {

        // Arrange
        Mockito.when(mockFormRepository.getAll())
                .thenReturn(Collections.emptyList());

        //Act
        List<SimulationForm> list = formService.getAll();

        //Assert
        Assertions.assertEquals(0, list.size());
    }

    @Test
    public void getById_ShouldReturn_WhenFormIdExists() {

        // Arrange
        SimulationForm simulationForm = new SimulationForm();
        Mockito.when(mockFormRepository.getById(1))
                .thenReturn(simulationForm);

        // Act
        SimulationForm returnedForm = formService.getById(1);

        //Assert
        Assertions.assertEquals(simulationForm, returnedForm);

    }

    @Test
    public void getById_ShouldThrow_WhenFormIdNotFound() {

        // Arrange
        Mockito.when(mockFormRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> formService.getById(1));
    }

    @Test
    public void calculateBaseAmount_ShouldReturnBaseAmount() {
        int cubicCapacity = 2500;
        int age = 24;

        //Arrange
        Mockito.when(mockFormRepository.calculateBaseAmount(Mockito.anyInt(), Mockito.anyInt())).thenReturn(892.86);

        //Act
        double baseCalculation = formService.calculateBaseAmount(cubicCapacity, age);

        //Assert
        Assertions.assertEquals(baseCalculation, 892.86);
    }

    @Test
    public void calculateTotalPrice_ShouldReturnTotalPrice() throws ParseException {

        SimulationFormDto simulationFormDto = getSimulationFormDto();

        int parsedDate = parseAge(simulationFormDto.getFirstRegistrationDate());

        Mockito.when(mockFormRepository.calculateBaseAmount(simulationFormDto.getCubicCapacity(),
                parsedDate)).thenReturn(862.86);
        double totalValue = formService.calculateTotalPrice(simulationFormDto);

        double expectedValue = 1195.92;

        Assertions.assertEquals(expectedValue, totalValue);
    }

    @Test
    public void saveForm_ShouldReturnForm() throws ParseException {
        CarModel carModel = new CarModel();
        carModel.setTitle("Test");
        Make make = new Make();
        make.setTitle("Make");
        carModel.setMake(make);


        SimulationFormDto simulationFormDto = getSimulationFormDto();
        SimulationForm expectedSimulationForm = toSimulationForm(simulationFormDto, carModel);

        Mockito.when(modelService.getById(Mockito.anyInt())).thenReturn(carModel);


        SimulationForm actual = formService.saveForm(simulationFormDto);
        Assertions.assertEquals(expectedSimulationForm, actual);
    }

}
