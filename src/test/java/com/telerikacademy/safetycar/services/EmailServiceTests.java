package com.telerikacademy.safetycar.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.TemplateEngine;


@ExtendWith(MockitoExtension.class)
public class EmailServiceTests {


    @InjectMocks
    EmailServiceImpl emailService;

    @Spy
    TemplateEngine mockTemplateEngine;

    @Mock
    JavaMailSender mailSender;

    @Test
    public void sendMail() {
        String email = "testUsername@test.test";
        String subject = "New subject";
        String text = "Text";

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject(subject);
        message.setText(text);

        emailService.sendSimpleMessage(email,subject,text);

        Mockito.verify(mailSender, Mockito.times(1)).send(message);
    }


}
