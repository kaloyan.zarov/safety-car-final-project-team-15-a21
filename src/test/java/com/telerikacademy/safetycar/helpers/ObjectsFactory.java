package com.telerikacademy.safetycar.helpers;

import com.telerikacademy.safetycar.models.CarModel;
import com.telerikacademy.safetycar.models.dtos.SimulationFormDto;
import com.telerikacademy.safetycar.models.SimulationForm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;

public class ObjectsFactory {

    public static SimulationFormDto getSimulationFormDto() {
        SimulationFormDto simulationFormDto = new SimulationFormDto();
        simulationFormDto.setModelId(23);
        simulationFormDto.setCubicCapacity(2400);
        simulationFormDto.setAccident(true);
        simulationFormDto.setDriverAge(18);
        simulationFormDto.setFirstRegistrationDate("2016-11-03");

        return simulationFormDto;
    }

    public static SimulationForm toSimulationForm(SimulationFormDto simulationFormDto, CarModel carModel)
            throws ParseException {
        SimulationForm simulationForm = new SimulationForm();
        simulationForm.setFirstRegistrationDate(new SimpleDateFormat("yyyy-MM-dd").parse(simulationFormDto.getFirstRegistrationDate()).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate());
        simulationForm.setDriverAge(simulationFormDto.getDriverAge());
        simulationForm.setAccidents(simulationFormDto.isAccident());
        simulationForm.setCubicCapacity(simulationFormDto.getCubicCapacity());
        simulationForm.setModel(carModel);
        return simulationForm;
    }
}
